<%@ page language="java" contentType="text/html; charset=utf-8"
         pageEncoding="utf-8"%>
<!-- 引入tag.jsp -->
<%@include file="../public/tag.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>修改密码</title>
    <!-- 引入css -->
    <link rel="stylesheet" type="text/css" href="${path}/resources/static/h-ui/css/H-ui.min.css" />
    <link rel="stylesheet" type="text/css" href="${path}/resources/static/h-ui.admin/css/H-ui.admin.css" />
    <link rel="stylesheet" type="text/css" href="${path}/resources/lib/Hui-iconfont/1.0.7/iconfont.css" />
    <link rel="stylesheet" type="text/css" href="${path}/resources/lib/icheck/icheck.css" />
    <link rel="stylesheet" type="text/css" href="${path}/resources/static/h-ui.admin/skin/default/skin.css" id="skin" />
    <link rel="stylesheet" type="text/css" href="${path}/resources/static/h-ui.admin/css/style.css" />

</head>
<body>
<article class="page-container">
    <div class="row cl">

        <label class="form-label col-xs-4 col-sm-3"><span class="c-red">*</span>新密码：</label>
        <div class="formControls col-xs-8 col-sm-9">
            <input type="text" placeholder="请输入密码" name="password" />
        </div>
    </div><br>
    <div class="row cl">
        <div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-3">
            <input class="btn btn-primary radius" type="button" value="&nbsp;&nbsp;提交&nbsp;&nbsp;" onclick="noThrough()">
        </div>
    </div>
</article>
<!-- 引入js -->
<script src="${path}/resources/js/jquery.js"></script>
<script src="${path}/resources/js/template.js"></script>
<script src="${path}/resources/js/common.js"></script>
<script src="${path}/resources/js/promise.min.js"></script>
<script src="${path}/resources/js/project.js"></script>
<script src="${path}/resources/js/jquery.common.js"></script>
<!-- 引入工具js -->
<script src="${path}/resources/js/jk_tools.js"></script>
<script type="text/javascript">
    function noThrough(){
        var id='${id}';
        var password = $("input[name=password]").val();
        if(cm.stringIsEmpty(id)){
            alert("参数缺失，请重新进入该页面");
            return false;
        }
        if(cm.stringIsEmpty(password)){
            alert("请输入密码");
            return false;
        }
        Project.ajax("/user/update/user",{id:id,password:password},null,true).ajaxOK(function(data) {
        },true);
    }
</script>
</body>
</html>