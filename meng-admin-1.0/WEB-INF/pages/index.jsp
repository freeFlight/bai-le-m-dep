<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
	
	<!-- 引入tag.jsp -->
	<%@include file="../pages/public/tag.jsp"%>
	<!DOCTYPE html>
<html lang="en">
<head>
<title>首页</title>
<link rel="stylesheet" type="text/css" href="${path}/resources/static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="${path}/resources/static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="${path}/resources/lib/Hui-iconfont/1.0.7/iconfont.css" />
<link rel="stylesheet" type="text/css" href="${path}/resources/lib/icheck/icheck.css" />
<link rel="stylesheet" type="text/css" href="${path}/resources/static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="${path}/resources/static/h-ui.admin/css/style.css" />
</head>
<body>
<!-- 顶栏 -->
	<header class="navbar-wrapper">
	<div class="navbar navbar-fixed-top">
		<div class="container-fluid cl"> <a class="logo navbar-logo f-l mr-10 hidden-xs" href="${path}/page/page/interface/to/index">到乐租后台管理</a> <a class="logo navbar-logo-m f-l mr-10 visible-xs" href="/aboutHui.shtml">H-ui</a> <span class="logo navbar-slogan f-l mr-10 hidden-xs"></span> <a aria-hidden="false" class="nav-toggle Hui-iconfont visible-xs" href="javascript:;">&#xe667;</a>
			<nav class="nav navbar-nav">
				<ul class="cl">
					<li class="dropDown dropDown_hover"><a href="javascript:;" class="dropDown_A"><i class="Hui-iconfont">&#xe600;</i> 新增 <i class="Hui-iconfont">&#xe6d5;</i></a>
						<ul class="dropDown-menu menu radius box-shadow">
							<li><a href="javascript:;" onclick="article_add('添加资讯','article-add.html')"><i class="Hui-iconfont">&#xe616;</i> 资讯</a></li>
							<li><a href="javascript:;" onclick="picture_add('添加资讯','picture-add.html')"><i class="Hui-iconfont">&#xe613;</i> 图片</a></li>
							<li><a href="javascript:;" onclick="product_add('添加资讯','product-add.html')"><i class="Hui-iconfont">&#xe620;</i> 产品</a></li>
							<li><a href="javascript:;" onclick="member_add('添加用户','member-add.html','','510')"><i class="Hui-iconfont">&#xe60d;</i> 用户</a></li>
						</ul>
					</li>
				</ul>
			</nav>
			<nav id="Hui-userbar" class="nav navbar-nav navbar-userbar hidden-xs">
				<ul class="cl">
					<li>超级管理员</li>
					<li class="dropDown dropDown_hover"> <a href="#" class="dropDown_A">${name} <i class="Hui-iconfont">&#xe6d5;</i></a>
						<ul class="dropDown-menu menu radius box-shadow">
							<li><a href="#">个人信息</a></li>
							<li><a href="#"  onclick="logout()">退出</a></li>
						</ul>
					</li>
					<li id="Hui-msg"> <a href="${path}/page/city/proxy/list" title="消息"><span class="badge badge-danger">${size}</span><i class="Hui-iconfont" style="font-size:18px">&#xe68a;</i></a> </li>
					<li id="Hui-skin" class="dropDown right dropDown_hover"> <a href="javascript:;" class="dropDown_A" title="换肤"><i class="Hui-iconfont" style="font-size:18px">&#xe62a;</i></a>
						<ul class="dropDown-menu menu radius box-shadow">
							<li><a href="javascript:;" data-val="default" title="默认（黑色）">默认（黑色）</a></li>
							<li><a href="javascript:;" data-val="blue" title="蓝色">蓝色</a></li>
							<li><a href="javascript:;" data-val="green" title="绿色">绿色</a></li>
							<li><a href="javascript:;" data-val="red" title="红色">红色</a></li>
							<li><a href="javascript:;" data-val="yellow" title="黄色">黄色</a></li>
							<li><a href="javascript:;" data-val="orange" title="绿色">橙色</a></li>
						</ul>
					</li>
				</ul>
			</nav>
		</div>
	</div>
</header>
	<!-- 菜单模块 -->
<aside class="Hui-aside">
	<input runat="server" id="divScrollValue" type="hidden" value="" />
	<div class="menu_dropdown bk_2">
		<%--<dl id="menu-admin">--%>
			<%--<dt><i class="Hui-iconfont">&#xe62d;</i> 系统管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>--%>
			<%--<dd>--%>
				<%--<ul>--%>
					<%--<li><a data-href="${path}/page/branch/list" data-title="后台用户" href="javascript:void(0)">后台用户</a></li>--%>
				<%--</ul>--%>
			<%--</dd>--%>
		<%--</dl>--%>
		<dl id="menu-admin">
			<dt><i class="Hui-iconfont">&#xe62d;</i> 背景图管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
			<dd>
				<ul>
					<li><a data-href="${path}/page/back/img/list" data-title="背景图列表" href="javascript:void(0)">背景图列表</a></li>
					<li><a data-href="${path}/page/back/img/add" data-title="添加背景图" href="javascript:void(0)">添加背景图</a></li>
				</ul>
			</dd>
		</dl>
		<dl id="menu-admin">
			<dt><i class="Hui-iconfont">&#xe62d;</i> 背景图类别管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
			<dd>
				<ul>
					<li><a data-href="${path}/page/back/img/class/list" data-title="类别列表" href="javascript:void(0)">类别列表</a></li>
					<li><a data-href="${path}/page/back/img/class/add" data-title="添加类别" href="javascript:void(0)">添加类别</a></li>
				</ul>
			</dd>
		</dl>
		<dl id="menu-article">
			<dt><i class="Hui-iconfont">&#xe616;</i> 公告-广播管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
			<dd>
				<ul>
					<li><a data-href="${path}/page/notice/list" data-title="公告-广播列表" href="javascript:void(0)">公告-广播列表</a></li>
					<li><a data-href="${path}/page/notice/hide/list" data-title="公告-广播列表" href="javascript:void(0)">隐藏公告-广播列表</a></li>
					<li><a data-href="${path}/page/notice/add" data-title="添加公告-广播" href="javascript:void(0)">添加公告-广播</a></li>
				</ul>
			</dd>
		</dl>
		<dl id="menu-picture">
			<dt><i class="Hui-iconfont">&#xe613;</i> 课程管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
			<dd>
				<ul>
					<li><a data-href="${path}/page/live/course/list" data-title="课程列表" href="javascript:void(0)">课程列表</a></li>
					<li><a data-href="${path}/page/live/course/base/stati" data-title="获取流量和带宽日统计结果" href="javascript:void(0)">获取流量和带宽日统计结果</a></li>
					<li><a data-href="${path}/page/live/course/bandwidth/stati" data-title="获取带宽实时统计结果" href="javascript:void(0)">获取带宽实时统计结果</a></li>
					<li><a data-href="${path}/page/live/course/play/user/stati" data-title="获取在线人数统计结果" href="javascript:void(0)">获取在线人数统计结果</a></li>
					<li><a data-href="${path}/page/live/course/play/user/day/stati" data-title="获取按天人数统计结果" href="javascript:void(0)">获取按天人数统计结果</a></li>
					<li><a data-href="${path}/page/live/course/play/user/month/stati" data-title="获取按月人数统计结果" href="javascript:void(0)">获取按月人数统计结果</a></li>
				</ul>
			</dd>
		</dl>

		<%--<dl id="menu-product">--%>
			<%--<dt><i class="Hui-iconfont">&#xe620;</i> 分类管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>--%>
			<%--<dd>--%>
				<%--<ul>--%>
					<%--<li><a data-href="${path}/page/forward/to/list/branch/car/brand" data-title="分类列表" href="javascript:void(0)">分类列表</a></li>--%>
					<%--<li><a data-href="${path}/page/forward/to/list/branch/car/info" data-title="添加分类" href="javascript:void(0)">添加分类</a></li>--%>
				<%--</ul>--%>
			<%--</dd>--%>
		<%--</dl>--%>
		<dl id="menu-product ">
		<dt><i class="Hui-iconfont">&#xe620;</i> 轮播图管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
		<dd>
			<ul>
				<li><a data-href="${path}/page/recommend/list" data-title="轮播列表" href="javascript:void(0)">轮播列表</a></li>
				<li><a data-href="${path}/page/recommend/add" data-title="添加轮播图" href="javascript:void(0)">添加轮播图</a></li>
			</ul>
		</dd>
	</dl>
		<dl id="menu-product ">
			<dt><i class="Hui-iconfont">&#xe620;</i> 广告管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
			<dd>
				<ul>
					<li><a data-href="${path}/page/advertisement/list" data-title="广告列表" href="javascript:void(0)">广告列表</a></li>
					<li><a data-href="${path}/page/advertisement/add" data-title="添加广告" href="javascript:void(0)">添加广告</a></li>
				</ul>
			</dd>
		</dl>
		<dl id="menu-comments">
			<dt><i class="Hui-iconfont">&#xe622;</i> 视频管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
			<dd>
				<ul>
					<li><a data-href="${path}/page/video/list" data-title="视频列表" href="javascript:;">视频列表</a></li>
					<li><a data-href="${path}/page/video/audit/list" data-title="待审核列表" href="javascript:;">待审核列表</a></li>
					<li><a data-href="${path}/page/video/notpass/list" data-title="未通过列表" href="javascript:;">未通过列表</a></li>
					<li><a data-href="${path}/page/video/add" data-title="添加视频" href="javascript:;">添加视频</a></li>
				</ul>
			</dd>
		</dl>
		<dl id="menu-member">
			<dt><i class="Hui-iconfont">&#xe60d;</i> 用户管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
			<dd>
				<ul>
					<li><a data-href="${path}/page/user/to/user/list" data-title="会员列表" href="javascript:;">会员列表</a></li>
				</ul>
			</dd>
		</dl>
		<dl id="menu-tongji">
			<dt><i class="Hui-iconfont">&#xe61a;</i> 群管理<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
			<dd>
				<ul>
					<li><a data-href="${path}/page/user/chat/room/list" data-title="用户群列表" href="javascript:;">用户群列表</a></li>
				</ul>
			</dd>
		</dl>
		<dl id="menu-system">
			<dt><i class="Hui-iconfont">&#xe62e;</i> 系统消息<i class="Hui-iconfont menu_dropdown-arrow">&#xe6d5;</i></dt>
			<dd>
				<ul>
					<li><a data-href="${path}/page/msg/list" data-title="消息列表" href="javascript:;">消息列表</a></li>
					<li><a data-href="${path}/page/msg/send" data-title="发布消息" href="javascript:;">发布消息</a></li>
				</ul>
			</dd>
		</dl>
	</div>
</aside>
<div class="dislpayArrow hidden-xs"><a class="pngfix" href="javascript:void(0);" onClick="displaynavbar(this)"></a></div>
<section class="Hui-article-box">
	<div id="Hui-tabNav" class="Hui-tabNav hidden-xs">
		<div class="Hui-tabNav-wp">
			<ul id="min_title_list" class="acrossTab cl">
				<li class="active"><span title="我的桌面" data-href="welcome.html">我的桌面</span><em></em></li>
			</ul>
		</div>
		<div class="Hui-tabNav-more btn-group"><a id="js-tabNav-prev" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d4;</i></a><a id="js-tabNav-next" class="btn radius btn-default size-S" href="javascript:;"><i class="Hui-iconfont">&#xe6d7;</i></a></div>
	</div>
	<div id="iframe_box" class="Hui-article">
		<div class="show_iframe">
			<div style="display:none" class="loading"></div>
			<iframe scrolling="yes" frameborder="0" src="home"></iframe>
		</div>
	</div>
</section>

<div class="contextMenu" id="myMenu1">
	<ul>
		<li id="open">Open </li>
		<li id="email">email </li>
		<li id="save">save </li>
		<li id="delete">delete </li>
	</ul>
</div>


<script type="text/javascript" src="${path}/resources/lib/jquery/1.9.1/jquery.min.js"></script> 
<script type="text/javascript" src="${path}/resources/lib/layer/2.1/layer.js"></script> 
<script type="text/javascript" src="${path}/resources/lib/jquery.contextmenu/jquery.contextmenu.r2.js"></script> 
<script type="text/javascript" src="${path}/resources/static/h-ui/js/H-ui.js"></script> 
<script type="text/javascript" src="${path}/resources/static/h-ui.admin/js/H-ui.admin.js"></script>

<script type="text/javascript">
$(function(){
	$(".Hui-tabNav-wp").contextMenu('myMenu1', {
		bindings: {
			'open': function(t) {
				alert('Trigger was '+t.id+'\nAction was Open');
			},
			'email': function(t) {
				alert('Trigger was '+t.id+'\nAction was Email');
			},
			'save': function(t) {
				alert('Trigger was '+t.id+'\nAction was Save');
			},
			'delete': function(t) {
				alert('Trigger was '+t.id+'\nAction was Delete')
			}
		}
	});
});
/*资讯-添加*/
function article_add(title,url){
	var index = layer.open({
		type: 2,
		title: title,
		content: url
	});
	layer.full(index);
}
/*图片-添加*/
function picture_add(title,url){
	var index = layer.open({
		type: 2,
		title: title,
		content: url
	});
	layer.full(index);
}
/*产品-添加*/
function product_add(title,url){
	var index = layer.open({
		type: 2,
		title: title,
		content: url
	});
	layer.full(index);
}
/*用户-添加*/
function member_add(title,url,w,h){
	layer_show(title,url,w,h);
}
//退出时清空session
function logout(){
	window.location.href="${path}/page/page/interface/to/login?name='logout'";
}

</script>
</body>
</html>
